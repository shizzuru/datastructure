#include <iostream>
#include <SFML/Graphics.hpp>

#include <fmt/core.h>
#include <fmt/format.h>

#include "../headers/paddle.h"
#include "../headers/ball.h"

void processKeyboard(sf::Time elapsed, Paddle *p);
sf::Text printInfos(Paddle p);
sf::Text printBallDebug(Ball bl);

const auto DIMENSIONS = sf::Vector2u(1024,720);
sf::Font font;
auto BALLDEBUG = false;

int main(void) {
    sf::RenderWindow app(sf::VideoMode(DIMENSIONS.x, DIMENSIONS.y), "Pong");
    // app.setFramerateLimit(60u);
    app.setVerticalSyncEnabled(true);
    if(!font.loadFromFile("./src/DroidSansMono.ttf"))
        return 1;

    sf::Clock clock = sf::Clock();

    Paddle pl = Paddle();
    Paddle pr = Paddle(sf::Vector2f(DIMENSIONS.x - 50.f, 0.f));
    Ball bl = Ball();
    bl.addPaddle(&pr);

    while(app.isOpen()) {
        auto elapsed = clock.restart();
        sf::Event e;
        while(app.pollEvent(e)) {
            if(e.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                app.close();
                
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Add))
                pr.Speed(10.f);
            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract))
                pr.Speed(-10.f);
            
            if(e.type == sf::Event::KeyPressed && e.key.code == sf::Keyboard::B) {
                BALLDEBUG = !BALLDEBUG;
            }
        }
        processKeyboard(elapsed, &pr);
        app.clear(sf::Color::Black);

        app.draw(printInfos(pr));
        if(BALLDEBUG)
            app.draw(printBallDebug(bl));
        app.draw(pr.getShape());
        app.draw(bl.updateShape(elapsed, DIMENSIONS));
        app.display();
    }
    return 0;
}

void processKeyboard(sf::Time elapsed, Paddle *p) {
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        p->Up(elapsed);
    else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        p->Down(elapsed);
}

sf::Text printInfos(Paddle p) {
    sf::Text text;
    text.setPosition(30,30);
    text.setFont(font);
    text.setString(
        fmt::format("Speed {}\nY {}", p.getSpeed(), p.getPosition().y)
    );

    return text;
}

sf::Text printBallDebug(Ball bl) {
    auto velocity = bl.getVelocity();
    auto position = bl.getPosition();
    sf::Text text;
    text.setPosition(30,650);
    text.setFont(font);
    text.setScale(.5f,.5f);
    text.setString(
        fmt::format("Speed X{}Y{}\nPosition X{}Y{}",
        velocity.x, velocity.y,
        position.x, position.y)
    );

    return text;
}