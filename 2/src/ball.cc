#include "../headers/ball.h"

Ball::Ball() : Ball(sf::Vector2f(500,500)) {}

Ball::Ball(sf::Vector2f pos) {
    this->_shape.setSize(sf::Vector2f(10,10));
    this->_shape.setFillColor(sf::Color::Cyan);
    this->_shape.setPosition(pos);
    this->_velocity = sf::Vector2f(-200,-100);
}

sf::Vector2f Ball::getPosition() {
    return this->_shape.getPosition();
}

sf::Vector2f Ball::getVelocity() {
    return this->_velocity;
}

sf::RectangleShape Ball::updateShape(sf::Time elapsed, sf::Vector2u dimensions) {
    auto currPos = this->_shape.getPosition();
    this->_processCollisions(dimensions, currPos);
    currPos = this->_shape.getPosition();

    this->_shape.setPosition(sf::Vector2f(
        currPos.x + this->_velocity.x * elapsed.asSeconds(),
        currPos.y + this->_velocity.y * elapsed.asSeconds()
    ));
    return _shape;
}

void Ball::addPaddle(Paddle* p) {
    this->_paddles.push_back(p);
}

void Ball::_processCollisions(sf::Vector2u dimensions, sf::Vector2f currPos) {
    auto gb = this->_shape.getGlobalBounds();

    if(currPos.y < 0) {
        this->_velocity.y *= -1;
    }
    else if(currPos.y+gb.height > dimensions.y) {
        this->_velocity.y *= -1;
    }
    
    if(currPos.x < 0) {
        this->_velocity.x *= -1;
    }
    else if(currPos.x+gb.width > dimensions.x) {
        this->_velocity.x *= -1;
    }

    // std::list<Paddle*>::iterator it;
    for(auto it = this->_paddles.begin(); it != this->_paddles.end(); it++) {
        auto paddleShape = (*it)->getShape();
        if(this->_shape.getGlobalBounds().intersects(paddleShape.getGlobalBounds())) {
            this->_shape.setPosition(paddleShape.getPosition().x-10, this->_shape.getPosition().y);
            this->_velocity.x *= -1;
        }
    }
}
