#include "../headers/paddle.h"

Paddle::Paddle() : Paddle(sf::Vector2f(0.f,0.f)) {}

Paddle::Paddle(sf::Vector2f pos) : _speed(200.f) {
    this->_shape.setSize(sf::Vector2f(15.f, 100.f));
    this->_shape.setFillColor(sf::Color::White);
    this->_shape.setPosition(pos);
}

void Paddle::Up(sf::Time elapsed) {
    this->_shape.move(0.f, -this->_speed*elapsed.asSeconds());
}

void Paddle::Down(sf::Time elapsed) {
    this->_shape.move(0.f, this->_speed*elapsed.asSeconds());
}

void Paddle::Speed(float modifier) {
    this->_speed += modifier;
}

float Paddle::getSpeed() {
    return this->_speed;
}

sf::Vector2f Paddle::getPosition() {
    return this->_shape.getPosition();
}

sf::RectangleShape Paddle::getShape() {
    return this->_shape;
}
