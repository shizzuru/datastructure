#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>
#include <list>

#include "paddle.h"

class Ball {
    public:
        Ball();
        Ball(sf::Vector2f);
        sf::Vector2f getPosition();
        sf::Vector2f getVelocity();
        sf::RectangleShape updateShape(sf::Time, sf::Vector2u);
        void addPaddle(Paddle*);
    private:
        sf::Vector2f _velocity;
        sf::RectangleShape _shape;
        std::list<Paddle*> _paddles;

        void _processCollisions(sf::Vector2u, sf::Vector2f);
};
