#pragma once

#include <SFML/Graphics.hpp>

class Paddle {
    public:
        Paddle();
        Paddle(sf::Vector2f pos);
        void Up(sf::Time);
        void Down(sf::Time);
        void Speed(float);
        float getSpeed();
        sf::Vector2f getPosition();
        sf::RectangleShape getShape();
    private:
        float _speed;
        sf::Vector2i _pos;
        sf::RectangleShape _shape;
};
