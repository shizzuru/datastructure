package main

import (
	"fmt"
	"strconv"
)

type human struct{}

// Input solves the game
func (h human) Input() (int, error) {
	var data string
	fmt.Printf("Guess number : ")
	fmt.Scanln(&data)
	ans, err := strconv.Atoi(data)
	if err != nil {
		return 0, err
	}
	return ans, nil
}
