package main

type linearSolver struct {
	i int
}

// Input solves the game
func (l *linearSolver) Input() (int, error) {
	l.i++
	return l.i, nil
}
