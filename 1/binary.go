package main

import (
	"math"
	"strings"
)

type binarySolver struct {
	i          int
	upperBound int
	lastResult string
}

type resultCollector interface {
	solver
	getResults(string)
}

// Input solves the game
func (b *binarySolver) Input() (int, error) {
	if b.lastResult == "" {
		b.i = b.upperBound / 2
		return b.i, nil
	}
	if strings.Contains(b.lastResult, "It's more") {
		b.i += int(math.Round(float64((b.upperBound - b.i)) / 2))
	} else if strings.Contains(b.lastResult, "It's less") {
		b.upperBound = b.i
		b.i /= 2
	}
	return b.i, nil
}

func (b *binarySolver) getResults(hint string) {
	h := strings.Split(hint, "- ")
	b.lastResult = h[1] // Stores only if it's more or less
}

/* ASSERT .(resultCollector)
if resultcollect -> envoi le hint */
