package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

const (
	upperBound = 100
	loops      = 1000
)

func main() {
	// h := &human{}

	linearTries := make([]int, 0)
	binaryTries := make([]int, 0)

	for i := 0; i < loops; i++ {
		l := &linearSolver{}
		g := game{toFind: rand.Intn(upperBound) + 1}
		tries, _ := g.autoPlay(l)
		linearTries = append(linearTries, tries)
	}

	for i := 0; i < loops; i++ {
		b := &binarySolver{upperBound: upperBound}
		g := game{toFind: rand.Intn(upperBound) + 1}
		tries, _ := g.autoPlay(b)
		binaryTries = append(binaryTries, tries)
	}
	fmt.Printf("Average linear tries : %v\n", average(linearTries))
	fmt.Printf("Average binary tries : %v\n", average(binaryTries))
}

func average(in []int) (avg int) {
	for _, v := range in {
		avg += v
	}
	return avg / len(in)
}
