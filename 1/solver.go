package main

type solver interface {
	Input() (int, error)
}
