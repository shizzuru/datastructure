package main

import (
	"fmt"
)

type game struct {
	toFind int
	i      int
}

// game returns int nil when the game is won
func (g *game) game(s solver) (int, string, error) {
	ans, err := s.Input()
	if err != nil {
		return 0, "", err
	}
	g.i++
	if ans > g.toFind {
		return g.i, fmt.Sprintf("%v - It's less \n", g.i), nil
	} else if ans < g.toFind {
		return g.i, fmt.Sprintf("%v - It's more \n", g.i), nil
	} else {
		return g.i, "", nil
	}
}

func (g game) autoPlay(s solver) (int, int) {
	for {
		tries, hint, err := g.game(s)
		if err != nil {
			continue
		}
		if hint == "" {
			return tries, g.toFind
		}
		if k, ok := s.(resultCollector); ok {
			k.getResults(hint)
		}
	}
}
